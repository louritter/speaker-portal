class TeamsController < ApplicationController
    def index
        @teams = Team.all
        @pagy, @teams = pagy(@teams, items: 5)
    end
    def show 
        @team = Team.find(params[:id])

        @speakers = @team.speakers
        @speakers = Speaker.all if params[:add_speaker].present?
        @speakers = Speaker.all.search(params[:query]) if params[:query].present?
        @pagy, @speakers = pagy(@speakers, items: 5)
    end

    def content
        
    end

    def add_speaker
        @team = Team.find(params[:id])
        @team.speakers << Speaker.find(params[:speaker])
        redirect_to team_path(@team)

    end

    def import_speakers
        @team = Team.find()
        Team.import(params[:file], @team.id)
        redirect_to team_path(@team), notice: "Speakers Imported"
    end
end
