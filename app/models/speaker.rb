class Speaker < ApplicationRecord
    
    require 'csv'
    include PgSearch::Model
    pg_search_scope :search, against: :name, using: { tsearch: { prefix: true }}
    has_many :team_speakers
    has_many :teams, :through => :team_speakers
    accepts_nested_attributes_for :team_speakers, :teams
end
