class TeamSpeaker < ApplicationRecord
    belongs_to :team
    belongs_to :speaker
    accepts_nested_attributes_for :speaker, :team
end
