class Team < ApplicationRecord
    has_many :team_speakers
    has_many :speakers, through: :team_speakers
    accepts_nested_attributes_for :team_speakers, :speakers

    def self.import(file, team)
        CSV.foreach(file.path, headers: true) do |row|
        team.speakers << Speaker.create!(row.to_hash)
        end
        self.save!

    end
end
