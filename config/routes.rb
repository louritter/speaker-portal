Rails.application.routes.draw do
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  resources :teams do 
    collection {post :import_speakers}
    get 'content', on: :member
    post 'add_speaker', on: :member


  end

  resources :speakers

  # Defines the root path route ("/")
   root "teams#index"
end
