class CreateTeamSpeakers < ActiveRecord::Migration[7.0]
  def change
    remove_reference :speakers, :team, index: true
    create_table :team_speakers do |t|
      t.references :team, null: false, foreign_key: true
      t.references :speaker, null: false, foreign_key: true
      t.timestamps
    end
  end
end
