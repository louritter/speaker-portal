# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
require 'faker'

User.destroy_all
User.create!(email: 'user1@test.com', password: 'password', password_confirmation: 'password')
10.times do 
    @team = Team.create!(name: Faker::Company.unique.name)
    10.times do 
        @team.speakers.create!(name: Faker::Name.unique.name)
    end
end
